<?php include_once 'inc/top.php';
        $kayttaja_id=1;
        $otsikko="";
        $teksti="";
        
        // Avataan tietokantayhteys. Nyt tietokantayhteys avataan aina, kun tämä sivu näytetään. Tähän voisi toki lisätä if-lauseen, joka
        //estää tietokannan avaamisen, jos käyttäjä avaan vain asiakkaan lisäyslomakkeen esiin (jolloin tietokantayhteyttä ei vielä tarvita).
        $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
        //Oletuksena PDO ei näytä mahdollisia virheitä, joten asetetaan "virhemoodi" päälle.
        $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);        
        
        if ($_SERVER['REQUEST_METHOD']==='POST') {
            try {
                // Luetaan tiedot lomakkeelta.
                $otsikko = filter_input(INPUT_POST, 'otsikko',FILTER_SANITIZE_STRING);
                $teksti = filter_input(INPUT_POST, 'teksti',FILTER_SANITIZE_STRING);
                
                $kysely = $tietokanta->prepare("INSERT INTO kirjoitus(kayttaja_id,otsikko,teksti) VALUES (:kayttaja_id,:otsikko,:teksti)");
                
                $kysely->bindValue(':otsikko',$otsikko,PDO::PARAM_STR);
                $kysely->bindValue(':teksti',$teksti,PDO::PARAM_STR);
                $kysely->bindValue(':kayttaja_id',$kayttaja_id,PDO::PARAM_INT);

                // Suoritetaan kysely ja tarkastetaan samalla mahdollinen virhe.
                if ($kysely->execute()) {
                    header('Location: index.php');
                }
                else {
                    print '<p>';
                    print_r($tietokanta->errorInfo());
                    print '</p>';
                }
                //print("<a href='index.php'>Etusivulle</a>");
            } catch (PDOException $pdoex) {
                print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
            }
        }
?>
    <div class="container">
        <div class="starter-template">
            <h1>Lisää kirjoitus</h1>
            <form action="<?php print($_SERVER['PHP_SELF']);?>" method="post">
                <div class="form-group">
                    <label for="text">Otsikko</label>
                    <input type="text" class="form-control" id="otsikko" name="otsikko">
                </div>
                <div class="form-group">
                    <label for="teksti">Teksti</label>
                    <textarea class="form-control" id="teksti" name="teksti" rows="3"></textarea>
                </div>
            <button type="submit" class="btn btn-primary">Tallenna</button>
            <button type="button" onclick="window.location='index.php';" class="btn btn-default">Peruuta</button>
            </form>
        </div>
    </div><!-- /.container -->

<?php include_once 'inc/bottom.php';?>
