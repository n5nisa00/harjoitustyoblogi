<?php include_once 'inc/top.php';?>

    <div class="container">

      <div class="starter-template">
          
          <?php
        try {
            // Avataan tietokantayhteys.
            $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
            //Oletuksena PDO ei näytä mahdollisia virheitä, joten asetetaan "virhemoodi" päälle.
            $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                       
            // Muodostetaan suoritettava sql-lause.
            $sql = 'SELECT * FROM kirjoitus ORDER BY id DESC';
           
            // Suoritetaan kysely tietokantaan.
            $kysely = $tietokanta->query($sql);
            
            if ($kysely) {

                while ($tietue = $kysely->fetch()) {  
                    print '<div class="kirjoitus">';
                    //print '<h4><b>' . $tietue['otsikko'] . '</b> ' . $tietue['paivays'] . '</h4>';
                    print '<h4><b>' . $tietue['otsikko'] . '</b>&nbsp' . date("d.m.y H.i", strtotime($tietue['paivays'])) . '</h4>';
                    print '<p>' . $tietue['teksti'] . '</p>';
                    print '<a href="poista.php?id=' . $tietue['id']. '"><span class="glyphicon glyphicon-trash"></span></a>';
                    print '<hr>';
                    print '</div>';
                }
            }
            else {
                print '<p>';
                print_r($tietokanta->errorInfo());
                print '</p>';
            }
            
        } catch (PDOException $pdoex) {
            print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
        }
        ?>
          
      </div>

    </div><!-- /.container -->
    
<?php include_once 'inc/bottom.php';?>