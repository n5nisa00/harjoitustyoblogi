<?php include_once 'inc/top.php';?>

    <div class="container">

        <div class="starter-template">
            <?php
            $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);

            try {

                // Avataan tietokantayhteys.
                $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
                //Oletuksena PDO ei näytä mahdollisia virheitä, joten asetetaan "virhemoodi" päälle.
                $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                // Muodostetaan parametroitu sql-kysely tiedon poistamista varten.
                $kysely = $tietokanta->prepare("DELETE FROM kirjoitus WHERE id=:id");

                $kysely->bindValue(':id',$id,PDO::PARAM_INT);

                // Suoritetaan kysely ja tarkastetaan samalla mahdollinen virhe.
                if ($kysely->execute()) {
                    print('<h4>Kirjoitus poistettu.</h4>');
                }
                else {
                    print '<h4>';
                    print_r($tietokanta->errorInfo());
                    print '</h4>';
                }
                print("<a href='index.php'>Takaisin etusivulle</a>");


            } catch (PDOException $pdoex) {
                print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage(). '</p>';
            }
            ?>
      </div>

    </div><!-- /.container -->
    
<?php include_once 'inc/bottom.php';?>